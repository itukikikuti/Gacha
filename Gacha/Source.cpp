#include "XLibrary11.hpp"
using namespace XLibrary11;

int Main()
{
	Camera camera;

	Sprite backgroundSprite(L"1/background.jpg");

	Sprite capsule1Sprite(L"1/capsule1.png");
	Sprite capsule2Sprite(L"1/capsule2.png");

	Sprite monstar1Sprite(L"1/1.png");
	Sprite monstar2Sprite(L"1/2.png");
	Sprite monstar3Sprite(L"1/3.png");
	Sprite monstar4Sprite(L"1/4.png");

	float value = 0.0f;

	monstar1Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
	monstar2Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
	monstar3Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
	monstar4Sprite.scale = Float3(0.0f, 0.0f, 0.0f);

	while (Refresh())
	{
		camera.Update();

		if (Input::GetKeyDown(VK_SPACE))
		{
			value = Random::Range(0.0f, 1.0f);

			capsule1Sprite.position = Float3(0.0f, 0.0f, 0.0f);
			capsule2Sprite.position = Float3(0.0f, 0.0f, 0.0f);

			monstar1Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
			monstar2Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
			monstar3Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
			monstar4Sprite.scale = Float3(0.0f, 0.0f, 0.0f);
		}

		backgroundSprite.angles.z += 0.5f;
		backgroundSprite.Draw();

		capsule1Sprite.position.y += 20.0f;
		capsule2Sprite.position.y -= 20.0f;

		capsule1Sprite.Draw();
		capsule2Sprite.Draw();

		monstar1Sprite.scale += Float3(0.1f, 0.1f, 0.1f);
		monstar2Sprite.scale += Float3(0.1f, 0.1f, 0.1f);
		monstar3Sprite.scale += Float3(0.1f, 0.1f, 0.1f);
		monstar4Sprite.scale += Float3(0.1f, 0.1f, 0.1f);

		if (monstar1Sprite.scale.x >= 1.0f)
		{
			monstar1Sprite.scale = Float3(1.0f, 1.0f, 1.0f);
		}
		if (monstar2Sprite.scale.x >= 1.0f)
		{
			monstar2Sprite.scale = Float3(1.0f, 1.0f, 1.0f);
		}
		if (monstar3Sprite.scale.x >= 1.0f)
		{
			monstar3Sprite.scale = Float3(1.0f, 1.0f, 1.0f);
		}
		if (monstar4Sprite.scale.x >= 1.0f)
		{
			monstar4Sprite.scale = Float3(1.0f, 1.0f, 1.0f);
		}

		if (value <= 1.0f && value > 0.99f)
		{
			monstar4Sprite.Draw();
		}
		if (value <= 0.99f && value > 0.9f)
		{
			monstar3Sprite.Draw();
		}
		if (value <= 0.9f && value > 0.5f)
		{
			monstar2Sprite.Draw();
		}
		if (value <= 0.5f)
		{
			monstar1Sprite.Draw();
		}
	}
}
